package SkyRails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by root on 5/14/15.
 */
@Service
public class ManualZoneService {

    private final ManualZoneRepository manualZoneRepository;

    @Autowired
    public ManualZoneService(ManualZoneRepository manualZoneRepository) {
        this.manualZoneRepository = manualZoneRepository;
    }

    //    Service layer methods

    public ManualZone create(ManualZone toPersist) {

        ManualZone persisted = manualZoneRepository.save(toPersist);

        //TODO - add functionality to be performed after a new manualZone was received

        return persisted;
    }

    public ManualZone delete(String id) {
        ManualZone toDelete = null;
        try {
            toDelete = findById(id);
        } catch (ManualZoneNotFoundException e) {
            e.printStackTrace();
        }
        manualZoneRepository.delete(toDelete);
        return toDelete;
    }

    public ManualZone getById(String id) {
        ManualZone manualZone = null;
        try {
            manualZone = findById(id);
        } catch (ManualZoneNotFoundException e) {
            e.printStackTrace();
        }
        return manualZone;
    }

    public ManualZone update(String id, ManualZone manualZone) {
        ManualZone toUpdate = null;
        try {
            toUpdate = findById(id);
            toUpdate.update(manualZone);
        } catch (ManualZoneNotFoundException e) {
            e.printStackTrace();
        }
        return manualZoneRepository.save(toUpdate);
    }

    public List<ManualZone> findAll() {
        return manualZoneRepository.findAll();
    }

    // --------------------------
    private ManualZone findById(String id) throws ManualZoneNotFoundException {
        Optional<ManualZone> result = manualZoneRepository.findOne(id);
        return result.orElseThrow(() -> new ManualZoneNotFoundException(id.toString()));
    }

    // ---------------------------
    public class ManualZoneNotFoundException extends Exception {

        public ManualZoneNotFoundException() {}

        public ManualZoneNotFoundException(String string) {
            super(string);
        }

    }

}
