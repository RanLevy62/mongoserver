package SkyRails;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.geojson.GeoJsonObject;
import org.springframework.data.annotation.Id;

/**
 * Created by root on 5/9/15.
 */
public class ManualZoneDto {

    private String id;

    GeoJsonObject geoFeature;

    boolean isClosed;

    public ManualZoneDto() {
    }

    // Getters and Setters

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @JsonProperty("isClosed")
    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    @JsonProperty("isClosed")
    public boolean isClosed() {
        return isClosed;
    }

    public void setGeoFeature(GeoJsonObject geoFeature) {
        this.geoFeature = geoFeature;
    }

    public GeoJsonObject getGeoFeature() {
        return geoFeature;
    }
}
