package SkyRails;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.geojson.GeoJsonObject;
import org.springframework.data.annotation.Id;

/**
 * Created by root on 5/9/15.
 */
public class ManualZone {

    @Id
    private String id;

    GeoJsonObject geoFeature;

    boolean isClosed;

    public ManualZone() {
    }

    public void update(ManualZone otherManualZone) {
        this.geoFeature = otherManualZone.getGeoFeature();
        this.isClosed = otherManualZone.isClosed;
    }

    // Getters and Setters

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setGeoFeature(GeoJsonObject geoFeature) {
        this.geoFeature = geoFeature;
    }

    public GeoJsonObject getGeoFeature() {
        return geoFeature;
    }
}
