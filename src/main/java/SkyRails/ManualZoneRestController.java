package SkyRails;

import org.geojson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Box;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 5/14/15.
 */
@RestController
@RequestMapping("/manualZone")
public class ManualZoneRestController {

    private final ManualZoneService service;

    @Autowired
    public ManualZoneRestController(ManualZoneService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ManualZoneDto create(@RequestBody @Valid ManualZoneDto manualZoneDto) {
        return convertToDto(service.create(convertFromDto(manualZoneDto)));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    ManualZoneDto delete(@PathVariable("id") String id) {
        return convertToDto(service.delete(id));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    ManualZoneDto get(@PathVariable("id") String id) {
        return convertToDto(service.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    List<ManualZoneDto> findAll() {
        return convertToDtos(service.findAll());
    }

    @RequestMapping(value = "{id}",method = RequestMethod.PUT)
    ManualZoneDto update(@PathVariable("id") String id, @RequestBody @Valid ManualZoneDto manualZoneDto) {
        return convertToDto(service.update(id, convertFromDto(manualZoneDto)));
    }
    @RequestMapping(value = "/search",method = RequestMethod.GET)
    List<ManualZoneDto> findInBox(@RequestParam(value = "min_lon") double min_lon,
                                 @RequestParam(value = "min_lat") double min_lat,
                                 @RequestParam(value = "max_lon") double max_lon,
                                 @RequestParam(value = "max_lat") double max_lat) {
        return null;
//        return manualZone2GeoJson(service.findInBox(new Box(new Point(min_lon, min_lat), new Point(max_lon, max_lat))));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleManualZoneNotFoundException(ManualZoneService.ManualZoneNotFoundException ex) {

    }

    // Dto Converters
    private ManualZone convertFromDto(ManualZoneDto manualZoneDto) {
        ManualZone manualZone = new ManualZone();

        if (manualZoneDto.getId() != null)
            manualZone.setId(manualZoneDto.getId());
        manualZone.setIsClosed(manualZoneDto.isClosed);
        manualZone.setGeoFeature(manualZoneDto.getGeoFeature());

        return manualZone;
    }

    private ManualZoneDto convertToDto(ManualZone manualZone) {
        ManualZoneDto manualZoneDto = new ManualZoneDto();

        manualZoneDto.setId(manualZone.getId());
        manualZoneDto.setIsClosed(manualZone.isClosed);
        manualZoneDto.setGeoFeature(manualZone.getGeoFeature());

        return manualZoneDto;
    }

    private List<ManualZoneDto> convertToDtos(List<ManualZone> manualZones) {
        List<ManualZoneDto> result = new ArrayList<>();

        for (ManualZone manualZone : manualZones) {
            result.add(convertToDto(manualZone));
        }

        return result;
    }

}
