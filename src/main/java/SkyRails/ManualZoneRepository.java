package SkyRails;

import org.springframework.data.geo.Box;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by root on 5/9/15.
 */
public interface ManualZoneRepository extends Repository<ManualZone, String> {
//    List<ManualZone> findByRadiusLessThan(float radius);
//    List<ManualZone> findByCenterWithin(Box box);

    void delete(ManualZone manualZone);

    Optional<ManualZone> findOne(String id);

    ManualZone save(ManualZone manualZone);

    List<ManualZone> findAll();

//    ManualZone update(ManualZone manualZone);

}
